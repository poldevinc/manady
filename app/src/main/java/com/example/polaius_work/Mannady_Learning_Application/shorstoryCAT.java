package com.example.polaius_work.Mannady_Learning_Application;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

public class shorstoryCAT extends AppCompatActivity implements View.OnTouchListener {
    public int evX, evY;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shorstory_cat);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ImageView iv = (ImageView) findViewById(R.id.image);
        ImageView ia = (ImageView) findViewById(R.id.image_areas);
        if (iv != null) {
            iv.setOnTouchListener(this);
        }
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        ia.setScaleType(ImageView.ScaleType.FIT_XY);

        final Intent home = new Intent(this, MainActivity.class);
        Button homebtn = (Button) findViewById(R.id.homebtn);

        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(home);
                finish();
            }
        });

        final Intent back = new Intent(this, read_sub.class);
        Button Back_main = (Button) findViewById(R.id.Back_main);

        Back_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(back);
                finish();
            }
        });





    }


    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }


    public boolean onTouch(View v, MotionEvent ev) {
        boolean handledHere = false;

        final int action = ev.getAction();


        switch (action) {

            case MotionEvent.ACTION_MOVE:
                handledHere = false;

                break;

            case MotionEvent.ACTION_DOWN:
                evX = (int) ev.getX();
                evY = (int) ev.getY();
                handledHere = true;

                break;


            case MotionEvent.ACTION_UP:

                if (ev.getX() < 0 || ev.getY() < 0) {


                } else {
                    int touchColor = getHotspotColor(R.id.image_areas, evX, evY);


                    ColorTool ct = new ColorTool();
                    int tolerance = 25;


                    if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {

                    } else {

                        if (evX < 0 || evY < 0) {

                        } else {
                            if (ct.closeMatch(Color.parseColor("#bb2322"), touchColor, tolerance)) {

                                Intent intent = new Intent(this, monkeyAndTheDog.class);
                                startActivity(intent);
                                finish();

                            } else if (ct.closeMatch(Color.parseColor("#1ecf7e"), touchColor, tolerance)) {

                                Intent intent = new Intent(this, smart_fly.class);
                                startActivity(intent);
                                finish();

                            }
                        }


                    } // end switch
                    handledHere = true;
                }


                break;


            default:
                handledHere = false;


        }
        return handledHere;
    }

    public int getHotspotColor(int hotspotId, int x, int y) {
        ImageView img = (ImageView) findViewById(hotspotId);
        if (img == null) {

            return 0;
        } else {
            img.setDrawingCacheEnabled(true);
            Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
            if (hotspots == null) {
                return 0;
            } else {
                img.setDrawingCacheEnabled(false);
                return hotspots.getPixel(x, y);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, read_sub.class);
        startActivity(intent);
        finish();
    }
}