package com.example.polaius_work.Mannady_Learning_Application;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class read extends AppCompatActivity {
    TableLayout table;
    TableLayout table2;
    TableRow.LayoutParams tableRowParams;
    String[] word;

    ArrayList<String> col1 = new ArrayList<String>();
    ArrayList<String> col2 = new ArrayList<String>();
    ArrayList<String> col3 = new ArrayList<String>();

    DatabaseHelper myDb;

    String lt = "";

    String hexc = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

/*

        String[] title = {
                "Abundance",
                "Anxiety",
                "Bruxism",
                "Discipline",
                "Drug Addiction"
        };
        LinearLayout m_ll = (LinearLayout) findViewById(R.id.llMain);
        for (int i = 0; i < title.length; i++) {
            String strToPrint = title [i] ;

            final TextView text = new TextView(this);
            text.setText(strToPrint);
            m_ll.addView(text);

            text.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // request your webservice here. Possible use of AsyncTask and ProgressDialog
                    // show the result here - dialog or Toast


                    Toast.makeText(getApplicationContext(), text.getText().toString(), Toast.LENGTH_SHORT).show();
                }

            });
        }



*/
/*

   */
/*     Find Tablelayout defined in main.xml *//*

        TableLayout tl = (TableLayout) findViewById(R.id.tablelayout);
*/
/* Create a new row to be added. *//*

        TableRow tr = new TableRow(this);
        tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
*/
/* Create a Button to be the row-content. *//*

        TextView b = new TextView(this);
        b.setText("Dynamic Button");
        b.setText("Dynamic Button2");
        b.setText("Dynamic Button3");
        b.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
*/
/* Add Button to row. *//*

        tr.addView(b);

*/
/* Add row to TableLayout. *//*

//tr.setBackgroundResource(R.drawable.sf_gradient_03);
        tl.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

*/
        final Intent home = new Intent(this, MainActivity.class);
        Button homebtn = (Button) findViewById(R.id.homebtn);


        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(home);
                finish();
            }
        });

        final Intent back = new Intent(this, read_sub.class);
        Button Back_main = (Button) findViewById(R.id.Back_main);

        Back_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(back);
                finish();
            }
        });



        myDb = new DatabaseHelper(this);
    viewAll();
        table = (TableLayout) findViewById(R.id.table1);

/*        table = new TableLayout(getApplicationContext());*/


        table.setStretchAllColumns(true);


      /*  tableRowParams = new TableRow.LayoutParams();*/
 /*      */

/*
        for (int i = 0; i < col1.size(); i++) {
            ReturnOfTheTable(i,table);
            ReturnOfTheTable(i,table2);


        }*/

        for (int i = 0; i < col1.size(); i++) {
            ReturnOfTheTable(i,table);



        }



        /*RelativeLayout container = (RelativeLayout) findViewById(R.id.rellayot);
        container.addView(table);
*/
    }


public void ReturnOfTheTable(int i,TableLayout tablethis){

    String col1S = col1.get(i);
    String col2S = col2.get(i);
    String col3S= col3.get(i);


    TableRow row= new TableRow(this);
    TableLayout.LayoutParams tableRowParams= new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);
    tableRowParams.setMargins(25, 25, 25, 25);
    row.setLayoutParams(tableRowParams);

            /*TableRow[] tableRow = new TableRow[col1.size()];
            tableRow[i] = new TableRow(getApplicationContext());
            tableRow[i].setGravity(Gravity.CENTER);
*/
    final TextView TVcol1 = new TextView(getApplicationContext());
    TVcol1.setGravity(Gravity.CENTER);
    TVcol1.setTextColor(Color.BLACK);
    TVcol1.setText(col1S);

    final TextView  TVcol2 = new TextView(getApplicationContext());
    TVcol2.setGravity(Gravity.CENTER);
    TVcol2.setTextColor(Color.BLACK);
    TVcol2.setText(col2S);

    final TextView   TVcol3 = new TextView(getApplicationContext());
    TVcol3.setGravity(Gravity.CENTER);
    TVcol3.setTextColor(Color.BLACK);
    TVcol3.setText(col3S);



    TVcol1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
    TVcol2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
    TVcol3.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);



    row.addView(TVcol1);
    row.addView(TVcol2);
    row.addView(TVcol3);









    TVcol1.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {

 /*           Toast.makeText(getApplicationContext(), TVcol1.getText().toString(), Toast.LENGTH_SHORT).show();*/
            Resources res = getBaseContext().getResources();
            int soundId = res.getIdentifier( TVcol1.getText().toString(), "raw", getBaseContext().getPackageName());
            MediaPlayer mediaPlayer = MediaPlayer.create(read.this, soundId);
            mediaPlayer.start();

        }

    });

    TVcol2.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {

/*
            Toast.makeText(getApplicationContext(), TVcol2.getText().toString(), Toast.LENGTH_SHORT).show();*/
            Resources res = getBaseContext().getResources();
            int soundId = res.getIdentifier( TVcol2.getText().toString(), "raw", getBaseContext().getPackageName());
            MediaPlayer mediaPlayer = MediaPlayer.create(read.this, soundId);
            mediaPlayer.start();
        }

    });

    TVcol3.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {

         /*   Toast.makeText(getApplicationContext(), TVcol3.getText().toString(), Toast.LENGTH_SHORT).show();*/
            Resources res = getBaseContext().getResources();
            int soundId = res.getIdentifier( TVcol3.getText().toString(), "raw", getBaseContext().getPackageName());
            MediaPlayer mediaPlayer = MediaPlayer.create(read.this, soundId);
            mediaPlayer.start();
        }

    });

    tablethis.addView(row,i);

}





    public void viewAll() {

        Cursor res = myDb.getAllWords();


        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
/*            buffer.append("Id :" + res.getString(0) + "\n");
             buffer.append("Name :" + res.getString(1) + "\n");
             buffer.append("Surname :" + res.getString(2) + "\n");
             buffer.append("Marks :" + res.getString(3) + "\n\n");*/
            col1.add(res.getString(1));
            col2.add(res.getString(2));
            col3.add(res.getString(3));
        }

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, read_sub.class);
        startActivity(intent);
        finish();
    }


}


