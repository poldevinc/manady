package com.example.polaius_work.Mannady_Learning_Application;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;



public class threeletterword extends AppCompatActivity implements View.OnTouchListener, RecognitionListener {


    DatabaseHelper myDb;
    ArrayList<String> leter = new ArrayList<String>();
    ArrayList<String> hexaC = new ArrayList<String>();
    String lt = "";
    public int evX, evY;
    Button Play_main;
    Button Stop_main;
    int length;
    MediaPlayer mediaPlayer_abc;
    MediaRecorder myAudioRecorder;
    private String outputFile = null;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    TextToSpeech t1;

    public Button PlayBack;
    public SpeechRecognizer speech = null;
    public Intent recognizerIntent;
    public String LOG_TAG = "VoiceRecognitionActivity";
    ArrayList<String> matches;


    String checkRecord = "record";
    String playback = "playback";


    ImageView iv ;
    ImageView ia ;


    Dialog settingsDialog;

    Button Next;
    String nums[]={"bag","bat","bed","but","can","cap","cut","fan","jet","led","man","map","mat","met","nap","net","pan","pat","red","sap","set","sun","tap","wet"};

    int pickupLinesItemIndex ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_threeletterword);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        myDb = new DatabaseHelper(this);
        Play_main = (Button) findViewById(R.id.play_main);
        Stop_main = (Button) findViewById(R.id.Stop_main);
        viewAll();






        iv = (ImageView) findViewById(R.id.image);
        ia = (ImageView) findViewById(R.id.image_areas);
        if (iv != null) {
            iv.setOnTouchListener(this);
        }
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        ia.setScaleType(ImageView.ScaleType.FIT_XY);



        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.ENGLISH);
                }
            }
        });


        speech = SpeechRecognizer.createSpeechRecognizer(this);
        speech.setRecognitionListener(this);
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,Locale.getDefault());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);

        final Intent home = new Intent(this, MainActivity.class);
        Button homebtn = (Button) findViewById(R.id.homebtn);

        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(home);
                finish();
            }
        });

        final Intent back = new Intent(this, read_sub.class);
        Button Back_main = (Button) findViewById(R.id.Back_main);

        Back_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(back);
                finish();
            }
        });

    }

    /**
     * Respond to the user touching the screen.
     * Change images to make things appear and disappear from the screen.
     */

    public boolean onTouch(View v, MotionEvent ev) {
        boolean handledHere = false;

        final int action = ev.getAction();

        matches= null;

        switch (action) {

            case MotionEvent.ACTION_MOVE:
                handledHere = false;

                break;

            case MotionEvent.ACTION_DOWN:
                evX = (int) ev.getX();
                evY = (int) ev.getY();

                handledHere = true;

                break;


            case MotionEvent.ACTION_UP:



                if (ev.getX() < 0 || ev.getY() < 0) {


                } else {


                    ColorTool ct = new ColorTool();
                    int tolerance = 25;
                    int touchColor2 = 0 ;

                    viewAll();

                    if(settingsDialog!=null ){
                        if (settingsDialog.isShowing()== true ){
                            touchColor2 = getHotspotColor(R.id.modal_hitbox, evX, evY);
                            if (ct.closeMatch(Color.TRANSPARENT, touchColor2, tolerance)) {
                                settingsDialog.cancel();

                            }
                        }else{
                            int touchColor = getHotspotColor(R.id.image_areas, evX, evY);


                            if(ct.closeMatch(Color.WHITE, touchColor, tolerance)){

                            }else{
                                for (int i = 0; i < leter.size(); i++) {
                                    if (evX < 0 || evY < 0) {

                                    } else {
                                        if (ct.closeMatch(Color.parseColor(hexaC.get(i)), touchColor, tolerance)) {
                                            lt = leter.get(i);
                                        }
                                    }

                                }

                                customModal(lt);
                            } // end switch
                            handledHere = true;
                        }

                    }else{
                        int touchColor = getHotspotColor(R.id.image_areas, evX, evY);

                        if(ct.closeMatch(Color.WHITE, touchColor, tolerance)){

                        }else{
                            for (int i = 0; i < leter.size(); i++) {
                                if (evX < 0 || evY < 0) {

                                } else {
                                    if (ct.closeMatch(Color.parseColor(hexaC.get(i)), touchColor, tolerance)) {
                                        lt = leter.get(i);
                                    }
                                }

                            }

customModal(lt);

                        } // end switch
                        handledHere = true;
                    }
                }


                break;


            default:
                handledHere = false;


        }
        return handledHere;
    }

    public int getHotspotColor(int hotspotId, int x, int y) {
        ImageView img = (ImageView) findViewById(hotspotId);
        if (img == null) {

            return 0;
        } else {
            img.setDrawingCacheEnabled(true);
            Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
            if (hotspots == null) {
                return 0;
            } else {
                img.setDrawingCacheEnabled(false);
                return hotspots.getPixel(x, y);
            }
        }
    }


    public void toast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    } // end toast


    /////////////
    public void viewAll() {

        Cursor res = myDb.getAllWords();


        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            //buffer.append("Id :" + res.getString(0) + "\n");
            // buffer.append("Name :" + res.getString(1) + "\n");
            // buffer.append("Surname :" + res.getString(2) + "\n");
            // buffer.append("Marks :" + res.getString(3) + "\n\n");
            leter.add(res.getString(1));
            hexaC.add(res.getString(2));
        }

    }

    //end of viewall()

    public void customModal(final String lt) {
        settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.custom_modal
                , null));


        settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        settingsDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        settingsDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        RelativeLayout modal_hitbox = (RelativeLayout) settingsDialog.findViewById(R.id.modal_hitbox);
        modal_hitbox.setOnTouchListener(this);





        final Button Play = (Button) settingsDialog.findViewById(R.id.Play);
        final Button Record = (Button) settingsDialog.findViewById(R.id.Record);
        final ImageView selected_img = (ImageView)settingsDialog.findViewById(R.id.selected_img);


        Next = (Button) settingsDialog.findViewById(R.id.Next);
        final Button Back = (Button) settingsDialog.findViewById(R.id.Back);
        final TextView textView2 = (TextView) settingsDialog.findViewById(R.id.textView2);
        textView2.setText(lt);

        selected_img.setImageResource(getResources().getIdentifier(lt, "drawable", getPackageName()));
        PlayBack = (Button) settingsDialog.findViewById(R.id.PlayBack);
        // if button is clicked, close the custom dialog

        Resources res = getBaseContext().getResources();
        int soundId = res.getIdentifier(lt, "raw", getBaseContext().getPackageName());
        MediaPlayer mediaPlayer = MediaPlayer.create(threeletterword.this, soundId);
        mediaPlayer.start();
        pickupLinesItemIndex = Arrays.asList(nums).indexOf(lt);

        if (pickupLinesItemIndex == nums.length-1){
            Next.setVisibility(View.INVISIBLE);

        }else if(pickupLinesItemIndex==0){
            Back.setVisibility(View.INVISIBLE);
        }
        else{
            Next.setVisibility(View.VISIBLE);
            Back.setVisibility(View.VISIBLE);
        }



        Next.setOnClickListener(new View.OnClickListener() {
            String lt2=lt;

            @Override
            public void onClick(View v) {
                if(pickupLinesItemIndex < nums.length)
                {

                    if (pickupLinesItemIndex == nums.length-2){
                        Next.setVisibility(View.INVISIBLE);

                    }else if(pickupLinesItemIndex <= -1){
                        Back.setVisibility(View.INVISIBLE);
                    }
                    else{
                        Next.setVisibility(View.VISIBLE);
                        Back.setVisibility(View.VISIBLE);
                    }


                    String pickupLine = nums[++pickupLinesItemIndex];

                    textView2.setText(pickupLine);

                    selected_img.setImageResource(getResources().getIdentifier(pickupLine, "drawable", getPackageName()));
                    Resources res = getBaseContext().getResources();
                    int soundId = res.getIdentifier(pickupLine, "raw", getBaseContext().getPackageName());
                    MediaPlayer mediaPlayer = MediaPlayer.create(threeletterword.this, soundId);
                    mediaPlayer.start();


                }


               /*
*/

            }
        });



        Back.setOnClickListener(new View.OnClickListener() {
            String lt2=lt;

            @Override
            public void onClick(View v) {
                if(pickupLinesItemIndex < nums.length)
                {

                    if (pickupLinesItemIndex == nums.length-2){
                        Next.setVisibility(View.INVISIBLE);

                    }else if(pickupLinesItemIndex <= 1){
                        Back.setVisibility(View.INVISIBLE);
                    }
                    else{
                        Next.setVisibility(View.VISIBLE);
                        Back.setVisibility(View.VISIBLE);
                    }


                    String pickupLine = nums[--pickupLinesItemIndex];

                    textView2.setText(pickupLine);

                    selected_img.setImageResource(getResources().getIdentifier(pickupLine, "drawable", getPackageName()));
                    Resources res = getBaseContext().getResources();
                    int soundId = res.getIdentifier(pickupLine, "raw", getBaseContext().getPackageName());
                    MediaPlayer mediaPlayer = MediaPlayer.create(threeletterword.this, soundId);
                    mediaPlayer.start();


                }


               /*
*/

            }
        });








        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashSet<MediaPlayer> mpSet = new HashSet<MediaPlayer>();
               /* int resID =getResources().getIdentifier(lt, "raw", getPackageName());

                MediaPlayer mediaPlayer=MediaPlayer.create(letter_activity.this,resID);
                mediaPlayer.start();
*/
                String lt = textView2.getText().toString();

                Resources res = getBaseContext().getResources();
                int soundId = res.getIdentifier(lt, "raw", getBaseContext().getPackageName());
                MediaPlayer mediaPlayer = MediaPlayer.create(threeletterword.this, soundId);
                mediaPlayer.start();

                Play.setEnabled(false);
                PlayBack.setEnabled(false);
                Record.setEnabled(false);

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    public void onCompletion(MediaPlayer mediaPlayer) {
                        Play.setEnabled(true);
                        PlayBack.setEnabled(true);
                        Record.setEnabled(true);
                    }
                });





         /*       String speak;*/
              /* if (lt.equals("a")) {
                   speak = "ae";
                   Toast.makeText(getApplicationContext(), speak, Toast.LENGTH_SHORT).show();
                   t1.speak(speak, TextToSpeech.QUEUE_FLUSH, null);


               }else if (lt.equals("z")){
                   speak = "zi";
                   Toast.makeText(getApplicationContext(), speak, Toast.LENGTH_SHORT).show();
                   t1.speak(speak, TextToSpeech.QUEUE_FLUSH, null);
               }else{
                   Toast.makeText(getApplicationContext(), lt,Toast.LENGTH_SHORT).show();
                   t1.speak(lt, TextToSpeech.QUEUE_FLUSH, null);
               }
*/

            }
        });

        Record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  speech.startListening(recognizerIntent);

                ctn++;*/


                if(checkRecord.equals("record")) {
                    try {

                        rv();
                        myAudioRecorder.prepare();
                        myAudioRecorder.start();
                        Record.setBackgroundResource(R.drawable.pause);
                        Play.setEnabled(false);
                        PlayBack.setEnabled(false);

                        checkRecord="Stop";

                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else if (checkRecord.equals("Stop")){
                    myAudioRecorder.stop();
                    myAudioRecorder.release();
                    PlayBack.setVisibility(View.VISIBLE);
                    Record.setBackgroundResource(R.drawable.record_button);
                    checkRecord="record";
                    Play.setEnabled(true);
                    PlayBack.setEnabled(true);

                }


            }
        });

        PlayBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                outputFile = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/myrec.3gp";
                File f = new File(outputFile);

                if (f.exists()) {

                    if(playback.equals("playback")) {

                        MediaPlayer m = new MediaPlayer();
                        try {

                            m.setDataSource(outputFile);
                            m.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        m.start();
                        Play.setEnabled(false);
                        PlayBack.setEnabled(false);
                        Record.setEnabled(false);
                        m.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                            public void onCompletion(MediaPlayer mp) {
                                Play.setEnabled(true);
                                PlayBack.setEnabled(true);
                                Record.setEnabled(true);
                            }
                        });



                    }else if (PlayBack.getText().toString().equals("PlayBack")){

                    }

                } else {
                    toast("there's no existing record");
                }

   /*     if (matches.equals(null)){



                }else{
            t1.speak( "you said " +matches.get(0), TextToSpeech.QUEUE_FLUSH, null);
        }
*/


            }
        });

        settingsDialog.show();

    }




    public void  rv(){
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/myrec.3gp";
        String fileName = outputFile;
        File myFile = new File(fileName);
        if(myFile.exists()){
            myFile.delete();
        }

        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);
    }




    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"say");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),"Not COmpatible",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        /*txtSpeechInput.setText(result.get(0));*/
                }
                break;
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (speech != null) {
            speech.destroy();

        }

    }

    @Override
    public void onBeginningOfSpeech() {


    }

    @Override
    public void onBufferReceived(byte[] buffer) {

    }

    @Override
    public void onEndOfSpeech() {


    }

    @Override
    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);

    }

    @Override
    public void onEvent(int arg0, Bundle arg1) {

    }

    @Override
    public void onPartialResults(Bundle arg0) {

    }

    @Override
    public void onReadyForSpeech(Bundle arg0) {

    }

    @Override
    public void onResults(Bundle results) {

        matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String s = matches.get(0).toLowerCase() ;

        if(lt.equals(s) || s.equals("letter " + lt)){
            t1.speak("correct!", TextToSpeech.QUEUE_FLUSH, null);
            PlayBack.setVisibility(View.VISIBLE);
        }else{
            t1.speak("wrong! you said" + s  +", try saying the word letter before saying the letter. for example letter " + lt, TextToSpeech.QUEUE_FLUSH, null);
            toast("wrong! you said" + s  +", try saying the word letter before saying the letter. for example letter " + lt);
        }

    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, flags.class);
        startActivity(intent);
        finish();
    }
}

