package com.example.polaius_work.Mannady_Learning_Application;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Polaius_work on 12/10/2016.
 */

public class recordVoice {
    static MediaRecorder myAudioRecorder;
    static String outputFile = null;

    public void rec (){

        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/myrec.3gp";
        String fileName = outputFile;
        File myFile = new File(fileName);
        if(myFile.exists()){
            myFile.delete();
        }

        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);


    }


}
