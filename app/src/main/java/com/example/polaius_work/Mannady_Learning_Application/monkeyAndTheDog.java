package com.example.polaius_work.Mannady_Learning_Application;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class monkeyAndTheDog extends AppCompatActivity implements View.OnTouchListener  {

    ViewPager viewPager;
    private float x1,x2;
    static final int MIN_DISTANCE = 200;
    ImageView iv ;
    int cnt=0;

    Button playbtn;
    MediaPlayer mediaPlayermonkey = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monkey_and_the_dog);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        iv = (ImageView) findViewById(R.id.image);
        playbtn = (Button) findViewById(R.id.playbtn);

        if(iv.getDrawable()==null){
            iv.setImageResource(R.drawable.monkeyanddogstorytitlepage);

            playbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int resID =getResources().getIdentifier("themonkeyandthedogaudiotitlepage", "raw", getPackageName());
                    mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                    mediaPlayermonkey.start();
                }
            });

        }




        if (iv != null) {
            iv.setOnTouchListener(this);
        }

        final Intent home = new Intent(this, MainActivity.class);
        Button homebtn = (Button) findViewById(R.id.homebtn);

        homebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayermonkey==null){

                }else if(mediaPlayermonkey.isPlaying()){
                    mediaPlayermonkey.stop();
                    mediaPlayermonkey.release();
                }
                startActivity(home);
                finish();
            }
        });


        final Intent back = new Intent(this, shorstoryCAT.class);
        Button Back_main = (Button) findViewById(R.id.Back_main);

        Back_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayermonkey==null){

                }else if(mediaPlayermonkey.isPlaying()){
                    mediaPlayermonkey.stop();
                    mediaPlayermonkey.release();
                }

                startActivity(back);
                finish();
            }
        });


    }

    public boolean onTouch(View v, MotionEvent ev) {
        boolean handledHere = false;

        final int action = ev.getAction();



        switch (action) {

            case MotionEvent.ACTION_MOVE:
                handledHere = false;

                break;

            case MotionEvent.ACTION_DOWN:

                x1 = ev.getX();

                handledHere = true;

                break;


            case MotionEvent.ACTION_UP:

                pauseall();
                x2 = ev.getX();
                float deltax = x2 - x1;

                if (x2 < x1 && deltax <= -MIN_DISTANCE){
           /*     toast(Float.toString(deltax));*/


                    if(cnt==0){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage1);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage1", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });



                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==1){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage2);



                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage2", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==2){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);

                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage3);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage3", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==3){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);



                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage4);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage4", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==4){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);



                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage5);
                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage5", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });


                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==5){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);



                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage6);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage6", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==6){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);



                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage7);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage7", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==7){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);



                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage8);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage8", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }else if(cnt==8){
                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);



                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_right);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);
                                iv.setImageResource(R.drawable.monkeyanddogstorypage9theend);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiotheend", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt ++;

                    }



     /*                   Toast.makeText(this, "x1 : "+x1+" x2: "+x2, Toast.LENGTH_LONG).show ();*/
                }else if (x2 > x1 && deltax >= MIN_DISTANCE){
                       /* toast(Float.toString(deltax));*/
                    if(cnt==1){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorytitlepage);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiotitlepage", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==2){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage1);
                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage1", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==3){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage2);
                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage2", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==4){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage3);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage3", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });
                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==5){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage4);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage4", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });
                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==6){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage5);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage5", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });
                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==7){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage6);

                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage6", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });
                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==8){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage7);
                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage7", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }else  if(cnt==9){

                        Animation animation1 =
                                AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
                        iv.startAnimation(animation1);
                        playbtn.startAnimation(animation1);


                        animation1.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }
                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation fadeOut = AnimationUtils.loadAnimation(monkeyAndTheDog.this, R.anim.slide_in_left);
                                iv.startAnimation(fadeOut);
                                playbtn.startAnimation(fadeOut);

                                iv.setImageResource(R.drawable.monkeyanddogstorypage8);
                                playbtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        pauseall();

                                        int resID =getResources().getIdentifier("themonkeyandthedogaudiopage8", "raw", getPackageName());

                                        mediaPlayermonkey= MediaPlayer.create(monkeyAndTheDog.this,resID);
                                        mediaPlayermonkey.start();
                                    }
                                });

                            }
                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });

                        cnt--;

                    }



                }


                else if (ev.getX() < 0 || ev.getY() < 0) {


                }

                break;


            default:
                handledHere = false;


        }
        return handledHere;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, shorstoryCAT.class);
        startActivity(intent);
        finish();
    }


public  void pauseall(){
  try{
      if(mediaPlayermonkey==null){

      }else if(mediaPlayermonkey.isPlaying()){
          mediaPlayermonkey.stop();
          mediaPlayermonkey.release();
      }else{

      }
  }catch (Exception e){

  }




}
    public void toast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    } // end toast


}
