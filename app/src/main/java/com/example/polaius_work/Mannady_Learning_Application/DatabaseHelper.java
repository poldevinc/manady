package com.example.polaius_work.Mannady_Learning_Application;

/**
 * Created by Polaius_work on 12/1/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ProgrammingKnowledge on 4/3/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "database.db";
    public static final String  TABLE_NAME_lETER= "leter";
    public static final String TABLE_NAME_NUMBERS= "numbers";
    public static final String australia= "australia";
    public static final String animals= "animals";
    public static final String words= "words";
    public static final String asia= "asia";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NAME";
    public static final String COL_3 = "SURNAME";
    public static final String COL_4 = "MARKS";
    static int count = 0;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME_lETER +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,alphabet_Leter TEXT,alphabet_hexa TEXT)");

        db.execSQL("create table " + TABLE_NAME_NUMBERS +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,Numbers TEXT,numbers_hexa TEXT)");
        db.execSQL("create table " + australia +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,australia TEXT,australia_hexa TEXT)");
        db.execSQL("create table " + asia +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,asia TEXT,asia_hexa TEXT)");
        db.execSQL("create table " + animals +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,animal TEXT,animal_hexa TEXT)");
        db.execSQL("create table " + words +" (ID INTEGER PRIMARY KEY AUTOINCREMENT,words TEXT,hexa TEXT)");

        if(count == 0){
            db.execSQL("INSERT INTO "+TABLE_NAME_lETER+"(`alphabet_Leter`,`alphabet_hexa`) VALUES" +
                    "('a','#fe0000')," +
                    "('b','#747474')," +
                    "('c','#ea49ef')," +
                    "('d','#071e10')," +
                    "('e','#f481f8')," +
                    "('f','#a3c1af')," +
                    "('g','#15294a')," +
                    "('h','#986199')," +
                    "('i','#74e862')," +
                    "('j','#3a5812')," +
                    "('k','#0d81c3')," +
                    "('l','#7ec8ab')," +
                    "('m','#cdc250')," +
                    "('n','#68321f')," +
                    "('o','#f3fd57')," +
                    "('p','#458b00')," +
                    "('q','#ffff00')," +
                    "('r','#ff7f00')," +
                    "('s','#ffb3b3')," +
                    "('t','#cacee6')," +
                    "('u','#f926a7')," +
                    "('v','#7326ff')," +
                    "('w','#8af436')," +
                    "('x','#008957')," +
                    "('y','#8d0478')," +
                    "('z','#149676')" );

            db.execSQL("INSERT INTO "+TABLE_NAME_NUMBERS+"(`Numbers`,`numbers_hexa`) VALUES" +
                    "('num_1','#fe0000')," +
                    "('num_2','#ea49ef')," +
                    "('num_3','#747474')," +
                    "('num_4','#000000')," +
                    "('num_5','#a3c1af')," +
                    "('num_6','#15294a')," +
                    "('num_7','#986199')," +
                    "('num_8','#cdc250')," +
                    "('num_9','#68321f')," +
                    "('num_10','#7326ff')");

            db.execSQL("INSERT INTO "+australia+"(`australia`,`australia_hexa`) VALUES" +
                    "('western_australia','#fe0000')," +
                    "('victoria','#747474')," +
                    "('tasmania','#ea49ef')," +
                    "('south_australia','#071e10')," +
                    "('queensland','#f481f8')," +
                    "('new_south_whales','#a3c1af')," +
                    "('northern_territory','#986199')");


            db.execSQL("INSERT INTO "+asia+"(`asia`,`asia_hexa`) VALUES" +
                    "('afghanistan','#fe0000')," +
                    "('bangladesh','#747474')," +
                    "('bhutan','#ea49ef')," +
                    "('brunei','#071e10')," +
                    "('cambodia','#f481f8')," +
                    "('china','#b6741d')," +
                    "('india','#ffcc00')," +
                    "('indonesia','#986199')," +
                    "('iran','#74e862')," +
                    "('iraq','#008e29')," +
                    "('japan','#0d81c3')," +
                    "('laos','#8f135f')," +
                    "('jordan','#7ec8ab')," +
                    "('malaysia','#68321f')," +
                    "('mongolia','#f3fd57')," +
                    "('nepal','#458b00')," +
                    "('north_korea','#ffff00')," +
                    "('oman','#ff7f00')," +
                    "('pakistan','#ffb3b3')," +
                    "('philippines','#cacee6')," +
                    "('saudi_arabia','#f926a7')," +
                    "('singapore','#7326ff')," +
                    "('south_korea','#c6fff1')," +
                    "('syria','#008957'),"+
                    "('armenia','#4591d8'),"+
                    "('azerbaijan','#6d92b5'),"+
                    "('bahrain','#62d19e'),"+
                    "('myanmar','#6b0a83'),"+
                    "('israel','#50734c'),"+
                    "('kazakhstan','#649e00'),"+
                    "('kuwait','#32416d'),"+
                    "('kyrgyzstan','#09173f'),"+
                    "('lebanon','#d3cc55'),"+
                    "('maldives','#471e4d'),"+
                    "('qatar','#11a6b8'),"+
                    "('sri_lanka','#04760c'),"+
                    "('taiwan','#9a012c'),"+
                    "('tajikistan','#a303a9'),"+
                    "('thailand','#afda04'),"+
                    "('timor_leste','#aeb29e'),"+
                    "('turkmenistan','#4d3c00'),"+
                    "('united_arab_emirates','#0d11b3'),"+
                    "('uzbekistan','#b0ff70'),"+
                    "('vietnam','#80f81e'),"+
                    "('yemen','#306703')");



            db.execSQL("INSERT INTO "+animals+"(`animal`,`animal_hexa`) VALUES" +
                    "('cat','#fe0000')," +
                    "('dog','#747474')," +
                    "('crocodile','#ea49ef')," +
                    "('goat','#071e10')," +
                    "('giraffe','#f481f8')," +
                    "('fish','#a3c1af')," +
                    "('deer','#15294a')," +
                    "('camel','#986199')," +
                    "('chicken','#74e862')," +
                    "('elephant','#3a5812')," +
                    "('duck','#0d81c3')," +
                    "('pig','#7ec8ab')," +
                    "('panda','#cdc250')," +
                    "('cow','#68321f')," +
                    "('bird','#f3fd57')," +
                    "('zebra','#458b00')," +
                    "('snake','#ffff00')," +
                    "('tiger','#ff7f00')," +
                    "('sheep','#ffb3b3')," +
                    "('shark','#cacee6')," +
                    "('rhino','#f926a7')," +
                    "('rabbit','#7326ff')," +
                    "('penguin','#8af436')," +
                    "('monkey','#008957')," +
                    "('lion','#8d0478')," +
                    "('kangaroo','#a9f0df')," +
                    "('horse','#7e9f18')" );







            db.execSQL("INSERT INTO "+words+"(`words`,`hexa`) VALUES" +
                    "('bag','#fe0000')," +
                    "('bat','#747474')," +
                    "('bed','#ea49ef')," +
                    "('but','#071e10')," +
                    "('can','#f481f8')," +
                    "('cap','#a3c1af')," +
                    "('cut','#15294a')," +
                    "('fan','#986199')," +
                    "('jet','#74e862')," +
                    "('led','#3a5812')," +
                    "('man','#0d81c3')," +
                    "('map','#7ec8ab')," +
                    "('mat','#cdc250')," +
                    "('met','#68321f')," +
                    "('nap','#f3fd57')," +
                    "('net','#458b00')," +
                    "('pan','#ffff00')," +
                    "('pat','#ff7f00')," +
                    "('red','#ffb3b3')," +
                    "('sap','#cacee6')," +
                    "('set','#f926a7')," +
                    "('sun','#7326ff')," +
                    "('tap','#8af436')," +
                    "('wet','#008957')");



            count++;
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME_lETER+","+TABLE_NAME_NUMBERS);
        onCreate(db);
    }

    public boolean insertData(String name,String surname,String marks) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,name);
        contentValues.put(COL_3,surname);
        contentValues.put(COL_4,marks);
        long result = db.insert(TABLE_NAME_lETER,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllAlphabet() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME_lETER,null);
        return res;
    }

    public Cursor getAllNumbers() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME_NUMBERS,null);
        return res;
    }

    public Cursor getAllaustralia() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+australia,null);
        return res;
    }

    public Cursor getAllCountryAsia() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+asia,null);
        return res;
    }

    public Cursor getAllAnimals() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+animals,null);
        return res;
    }

    public Cursor getAllWords() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+words,null);
        return res;
    }


    public boolean updateData(String id,String name,String surname,String marks) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,id);
        contentValues.put(COL_2,name);
        contentValues.put(COL_3,surname);
        contentValues.put(COL_4,marks);
        db.update(TABLE_NAME_lETER, contentValues, "ID = ?",new String[] { id });
        return true;
    }

    public Integer deleteData (String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME_lETER, "ID = ?",new String[] {id});
    }
}