package com.example.polaius_work.Mannady_Learning_Application;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

public class splash_screen extends AppCompatActivity implements View.OnTouchListener {
    public int evX, evY;
    MediaPlayer mediaPlayer;
    Dialog settingsDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


        ///

        ImageView iv = (ImageView) findViewById(R.id.image);
        ImageView ia = (ImageView) findViewById(R.id.image_areas);
        if (iv != null) {
            iv.setOnTouchListener(this);
        }
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        ia.setScaleType(ImageView.ScaleType.FIT_XY);

        int resID =getResources().getIdentifier("home_category_screen_background_music", "raw", getPackageName());

         mediaPlayer=MediaPlayer.create(splash_screen.this,resID);
        mediaPlayer.start();

    }


    public boolean onTouch(View v, MotionEvent ev) {
        boolean handledHere = false;

        final int action = ev.getAction();


        switch (action) {

            case MotionEvent.ACTION_MOVE:
                handledHere = false;

                break;

            case MotionEvent.ACTION_DOWN:
                evX = (int) ev.getX();
                evY = (int) ev.getY();
                handledHere = true;

                break;


            case MotionEvent.ACTION_UP:

                if (ev.getX() < 0 || ev.getY() < 0) {


                } else {
                    int touchColor = getHotspotColor(R.id.image_areas, evX, evY);


                    ColorTool ct = new ColorTool();
                    int tolerance = 25;


                    if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {

                    } else {

                        if (evX < 0 || evY < 0) {

                        } else {
                            if (ct.closeMatch(Color.parseColor("#00ff4e"), touchColor, tolerance)) {

                                Intent intent = new Intent(this, MainActivity.class);
                                startActivity(intent);
                                finish();


                            } else if (ct.closeMatch(Color.parseColor("#0012ff"), touchColor, tolerance)) {
                                customModal();
                            }
                        }


                    } // end switch
                    handledHere = true;
                }


                break;


            default:
                handledHere = false;


        }
        return handledHere;
    }

    public int getHotspotColor(int hotspotId, int x, int y) {
        ImageView img = (ImageView) findViewById(hotspotId);
        if (img == null) {

            return 0;
        } else {
            img.setDrawingCacheEnabled(true);
            Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
            if (hotspots == null) {
                return 0;
            } else {
                img.setDrawingCacheEnabled(false);
                return hotspots.getPixel(x, y);
            }
        }
    }

    public void customModal() {
        settingsDialog = new Dialog(this);
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.activity_credits
                , null));


        settingsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        // if button is clicked, close the custom dialog




        Button closeVideo = (Button) settingsDialog.findViewById(R.id.closeVieo);



        closeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.cancel();
            }
        });





        settingsDialog.show();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Runtime.getRuntime().gc();
    }


    @Override
    public void onPause()
    {
        super.onPause();
        if(mediaPlayer.isPlaying())
            mediaPlayer.pause();
        else
            return;
    }

    @Override
    public void onStop()
    {
        super.onStop();
        if(mediaPlayer.isPlaying())
            mediaPlayer.pause();
        else
            return;
    }


}

