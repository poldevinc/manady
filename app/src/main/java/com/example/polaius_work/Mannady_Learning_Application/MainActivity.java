package com.example.polaius_work.Mannady_Learning_Application;



import android.content.Intent;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    public int evX, evY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

       ///

        ImageView iv = (ImageView) findViewById(R.id.image);
        ImageView ia = (ImageView) findViewById(R.id.image_areas);
        if (iv != null) {
            iv.setOnTouchListener(this);
        }
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        ia.setScaleType(ImageView.ScaleType.FIT_XY);
    }

  public void viewAll() {
//
//        Cursor res = myDb.getAllData();
//        if (res.getCount() == 0) {
//            // show message
//            showMessage("Error","Nothing found");
//
//            return;
//        }
//
//        StringBuffer buffer = new StringBuffer();
//        while (res.moveToNext()) {
//           // buffer.append("Id :" + res.getString(0) + "\n");
//           // buffer.append("Name :" + res.getString(1) + "\n");
//           // buffer.append("Surname :" + res.getString(2) + "\n");
//           // buffer.append("Marks :" + res.getString(3) + "\n\n");
//
////            category.setText(res.getString(1));
//  //          file_location.setText(res.getString(1));
//    //        file_format.setText(res.getString(2)+res.getString(1)+res.getString(3));
//        }
//
//        showMessage("Data",buffer.toString());



    }

    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }


    public boolean onTouch(View v, MotionEvent ev) {
        boolean handledHere = false;

        final int action = ev.getAction();



        switch (action) {

            case MotionEvent.ACTION_MOVE:
                handledHere = false;

                break;

            case MotionEvent.ACTION_DOWN:
                evX = (int) ev.getX();
                evY = (int) ev.getY();
                handledHere = true;

                break;


            case MotionEvent.ACTION_UP:

                if (ev.getX() < 0 || ev.getY() < 0) {


                } else {
                    int touchColor = getHotspotColor(R.id.image_areas, evX, evY);


                    ColorTool ct = new ColorTool();
                    int tolerance = 25;


                    viewAll();


                    if (ct.closeMatch(Color.WHITE, touchColor, tolerance)) {

                    } else {

                        if (evX < 0 || evY < 0) {

                        } else {
                            if (ct.closeMatch(Color.parseColor("#b14748"), touchColor, tolerance)) {

                                Intent intent = new Intent(this, letter_activity.class);
                                startActivity(intent);
                                finish();

                            }else if (ct.closeMatch(Color.parseColor("#28a24a"), touchColor, tolerance)){

                                Intent intent = new Intent(this, numbers.class);
                                startActivity(intent);
                                finish();

                            }else if (ct.closeMatch(Color.parseColor("#3868c9"), touchColor, tolerance)){

                                Intent intent = new Intent(this, flags.class);
                                startActivity(intent);
                                finish();

                            }else if (ct.closeMatch(Color.parseColor("#5b03fc"), touchColor, tolerance)){
                                Intent intent = new Intent(this, animal.class);
                                startActivity(intent);
                                finish();

                            }else if (ct.closeMatch(Color.parseColor("#fffc00"), touchColor, tolerance)){
                                Intent intent = new Intent(this, read_sub.class);
                                startActivity(intent);
                                finish();


                            }
                        }




                    } // end switch
                    handledHere = true;
                }


                break;


            default:
                handledHere = false;


        }
        return handledHere;
    }

    public int getHotspotColor(int hotspotId, int x, int y) {
        ImageView img = (ImageView) findViewById(hotspotId);
        if (img == null) {

            return 0;
        } else {
            img.setDrawingCacheEnabled(true);
            Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
            if (hotspots == null) {
                return 0;
            } else {
                img.setDrawingCacheEnabled(false);
                return hotspots.getPixel(x, y);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, splash_screen.class);
        startActivity(intent);
        finish();
    }

}
